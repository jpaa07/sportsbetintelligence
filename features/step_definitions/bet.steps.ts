import { SoccerUtil } from "../../src/utilities/soccer-utilities";
import { JsonAccess } from "../../src/utilities/json-access";
import { Open } from "serenity-js/lib/serenity-protractor";
import { Login } from "../../src/screenplay/tasks/login/login";
import { CloseAdds } from "../../src/screenplay/tasks/login/close_initial_adds";
import { NavigateToSport } from "../../src/screenplay/tasks/bet/navigate_to_sport_bets";
import { Actor } from "serenity-js/lib/screenplay";
import { SelectCompetition } from "../../src/screenplay/tasks/bet/select_competition";
import { EventCard } from "../../src/models/event_card";
import { CommonUtilities } from "../../src/utilities/common_utilities";
import { MakeABet } from "../../src/screenplay/tasks/bet/make_a_bet";

export = async function Bet() {
    let generalActor: Actor;

    this.Given(/^(.*) login playfulbet application to make a (.*) bet$/,
        async function (actor: string, sport: string) {
        generalActor = this.stage.theActorCalled(actor);
        const actorCredentials = await JsonAccess.getActorCredentials(actor, sport);
        return generalActor.attemptsTo(
            await Open.browserOn(await JsonAccess.getLoginUrl()),
            await Login.with(actorCredentials.email, actorCredentials.password),
            await CloseAdds.perform()
        );
        
    });

    this.When(/^he goes to soccer module and choose the league: (.*)$/, async function (competition: string) {
        return generalActor.attemptsTo(
            await NavigateToSport.soccer(),
            await SelectCompetition.goTo(competition)
        );
    });

    this.Then(/^he should bet on (.*) matches with a win probability greater than: (.*)%$/, async function (competition: string, probability: number) {
        const events: EventCard[] =  await CommonUtilities.getEventCards();
        let teamToBet;
        for(const event of await events) {
            await generalActor.attemptsTo(Open.browserOn(event.$url));
            let appPrediction = await CommonUtilities.getAppPredictions();
            let inactiveTeams: string[] = await JsonAccess.getInactiveTeams(competition);
            console.log(appPrediction);
            if (!(inactiveTeams.indexOf(appPrediction.teams.homeName) >= 0) && !(inactiveTeams.indexOf(appPrediction.teams.awayName) >= 0)){
                let resultPrediction = await SoccerUtil.getResultPrediction(5, await JsonAccess.getApiTeamName(competition, appPrediction.teams.homeName),
                    await JsonAccess.getApiTeamName(competition, appPrediction.teams.awayName), competition);
                teamToBet = await SoccerUtil.getFinalResult(probability, resultPrediction.home,resultPrediction.away,resultPrediction.match, appPrediction.predictions);
                await generalActor.attemptsTo(await MakeABet.perform(teamToBet, 200));
            }    
        }
    });
}