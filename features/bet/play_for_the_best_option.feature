Feature: play bet for the best option
    As a user
    I want to play for the option than has most probability to win
    So that double my money

    Scenario: make a soccer bet
        Given soccerbettor01 login playfulbet application to make a soccer bet
        When he goes to soccer module and choose the league: serie-a
        When he should bet on serie-a matches with a win probability greater than: 75%