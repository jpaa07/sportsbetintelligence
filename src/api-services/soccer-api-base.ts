import * as request from 'request-promise';
import { SeasonHeadToHeadResponse } from './soccer-response-models/season-head-to-head-response';

export class ApiBase {
    
    private static baseURL = `https://sportsop-soccer-sports-open-data-v1.p.mashape.com/v1`;
    private static defaultHeaders = {
        'X-Mashape-Key': 'iJU4omqZbhmshS0dUys6im8AvhjNp13lYlAjsnjHIbIWP1tNN2',
        'Accept': 'application/json'
    };

    public static async getSeasonHeadToHead (league: string, season: string, team1: string, team2: string): Promise<SeasonHeadToHeadResponse> {
        const url = `${this.baseURL}/leagues/${league}/seasons/${season}/rounds?team_1_slug=${team1}&team_2_slug=${team2}`;
        try {
            let response =  await request({
                url: url,
                method: 'GET',
                headers: this.defaultHeaders,
                rejectUnauthorized: false,
                json: true
            });
            const data: SeasonHeadToHeadResponse = await response.data;
            return data;
        } catch(e) {
            console.log(e);            
        }
        
    } 

    public static async getSeasonMatchesForATeam (league: string, season: string, teamIdentifier: string): Promise<SeasonHeadToHeadResponse> {
        const url = `${this.baseURL}/leagues/${league}/seasons/${season}/rounds?team_identifier=${teamIdentifier}`
        const response =  await request({
			url: url,
			method: 'GET',
			headers: this.defaultHeaders,
			rejectUnauthorized: false,
			json: true
        });
        const data: SeasonHeadToHeadResponse = await response.data;
        return data;
    } 


}
