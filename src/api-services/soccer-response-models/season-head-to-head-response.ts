import { BaseResponse } from "./base-response";

export class SeasonHeadToHeadResponse extends BaseResponse {
    
    public data: any;
    public rounds: Round[];

    constructor(rounds: Round[]){
        super();
        this.rounds = rounds;
    }
}

export class Round {
    public identifier: string;
    public name: string;
    public start_date: string;
    public end_date: string;
    public round_slug: string;
    public match_slug: string;
    public date_match: string;
    public home_team: string;
    public away_team: string;
    public match_result: string;
}