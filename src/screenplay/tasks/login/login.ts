import { Task, Actor } from "serenity-js/lib/screenplay";
import { Enter, Click } from "serenity-js/lib/serenity-protractor";
import { LoginPage } from "../../ui/login.page";

export class Login implements Task {

    static with(email: string, password: string) {
        return new Login(email,password);
    }

    async performAs(actor: Actor) {
        return actor.attemptsTo(
            await Enter.theValue(this.email).into(LoginPage.usernameField),
            await Enter.theValue(this.password).into(LoginPage.passwordField),
            await Click.on(LoginPage.loginButton)
        );
    }

    constructor(private email: string, private password: string) {}
}