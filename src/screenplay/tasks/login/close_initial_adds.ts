import { Task, Actor } from "serenity-js/lib/screenplay";
import { Click } from "serenity-js/lib/serenity-protractor";
import { HomePage } from "../../ui/home.page";
import { Duration, ActiveWait, Is } from "../../interactions/wait";
import { Accept } from "../../interactions/accept";

export class CloseAdds implements Task {

    static perform() {
        return new CloseAdds();
    }

    async performAs(actor: Actor) {   
        //const activeWait = new ActiveWait(Duration.ofSeconds(60));  
        await actor.attemptsTo(
            await Click.on(HomePage.blockNotificationButton),
            //await activeWait.until(HomePage.closeAddsModalButton,Is.clickable()),
            //await Click.on(HomePage.closeAddsModalButton),
            await Accept.dailyBonus()
        );   
    }

    constructor() {}
}