import { Task, Actor } from "serenity-js/lib/screenplay";
import { Click, Target } from "serenity-js/lib/serenity-protractor";
import { Wait, Duration, ActiveWait, Is } from "../../interactions/wait";
import { SportPage } from "../../ui/sport.page";
import { Select } from "../../interactions/select";
import { JsonAccess } from "../../../utilities/json-access";
import { browser, by } from "protractor";
import { equal } from "assert";

export class SelectCompetition implements Task {

    public static sportTarget: Target;

    static async goTo(competition: string) {
        return new SelectCompetition(await JsonAccess.getPFBcompetitionName(competition));
    }
 
    async performAs(actor: Actor) {
        const activeWait = new ActiveWait(Duration.ofSeconds(60));
        const competitionTitle = Target.the('competition title label').located(by.cssContainingText('.leader--s.trailer--s',this.competition));

        return actor.attemptsTo(
            await activeWait.until(SportPage.sportCompetitionsDropdow,Is.visible()),
            await Click.on(SportPage.sportCompetitionsDropdow),
            await Select.competition(this.competition),
            await Click.on(SportPage.filterByCompetitionButton),
            await activeWait.until(competitionTitle,Is.visible())
        );
    }

    constructor(private competition: string) {
    }
}