import { Task, Actor } from "serenity-js/lib/screenplay";
import { Click, Target } from "serenity-js/lib/serenity-protractor";
import { LeftNavbarPage } from "../../ui/left_navbar.page";
import { Wait, Duration, ActiveWait, Is } from "../../interactions/wait";

export class NavigateToSport implements Task {

    public static sportTarget: Target;

    static soccer() {
        this.sportTarget = LeftNavbarPage.soccerBetsButton;
        return new NavigateToSport();
    }

    static bascketball() {
        this.sportTarget = LeftNavbarPage.bascketBetsButton;
        return new NavigateToSport();
    }

    static tennis() {
        this.sportTarget = LeftNavbarPage.tennisBetsButton;
        return new NavigateToSport();
    }

    async performAs(actor: Actor) {
        const activeWait = new ActiveWait(Duration.ofSeconds(60));
        return actor.attemptsTo(
            await activeWait.until(NavigateToSport.sportTarget,Is.clickable()),
            await Click.on(NavigateToSport.sportTarget)
        );
    }

    constructor() {
    }
}