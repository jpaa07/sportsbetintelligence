import { Task, Actor } from "serenity-js/lib/screenplay";
import { Click, Target, Enter, Clear } from "serenity-js/lib/serenity-protractor";
import { Duration, ActiveWait, Is } from "../../interactions/wait";
import { by, element, ElementFinder } from "protractor";
import { BetPage } from "../../ui/bet.page";

export class MakeABet implements Task {

    public static sportTarget: Target;

    static async perform(side: number, money) {
        return new MakeABet(side, money);
    }
 
    async performAs(actor: Actor) {
        if(this.side != -1){
            const activeWait = new ActiveWait(Duration.ofSeconds(90));
            const options: ElementFinder[] = await element.all(by.className('bet-option'));
            await options[this.side].click();

            return actor.attemptsTo(
                await activeWait.until(BetPage.pointsInput,Is.visible()),
                await Clear.theValueOf(BetPage.pointsInput),
                await Enter.theValue(this.money).into(BetPage.pointsInput),
                await Click.on(BetPage.playBetButton),
                await activeWait.until(BetPage.optionSelected,Is.visible())
            );
        }
    }

    constructor(private side: number, private money: number) {
    }
}