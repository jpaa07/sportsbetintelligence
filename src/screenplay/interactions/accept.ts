import { HomePage } from "../ui/home.page";
import { Interaction, Activity, UsesAbilities } from "serenity-js/lib/screenplay";
import { BrowseTheWeb } from "serenity-js/lib/serenity-protractor";
import { browser } from "protractor";

export class Accept{

    static async dailyBonus() { 
            return new DailyBonus();
    }
}

export class DailyBonus implements Interaction {

    async performAs(actor: UsesAbilities): Promise<void> {
        if(await browser.isElementPresent(HomePage.acceptDailyBonusButton.resolveUsing(browser.element))){
            return BrowseTheWeb.as(actor)
            .locate(HomePage.acceptDailyBonusButton)
            .click();
        }
    }

    constructor(){} 
}

