import { Interaction, UsesAbilities } from "serenity-js/lib/screenplay";
import { BrowseTheWeb, Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export class Select{

    static async competition(competition: string) { 
            return new Competition(competition);
    }
}

export class Competition implements Interaction {

    async performAs(actor: UsesAbilities): Promise<void> {

        const competitionOption: Target = Target.the('Competition option')
            .located(by.cssContainingText('.event-nav-competition-item', this.competition));

        return BrowseTheWeb.as(actor)  
            .locate(competitionOption)
            .click();
    }

    constructor(private competition: string){}
}
