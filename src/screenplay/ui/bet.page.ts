import { Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export const BetPage = {
    pointsInput: Target.the('points to bet input').located(by.id('points')),
    playBetButton: Target.the('play bet button').located(by.id('play-action')),
    optionSelected: Target.the('bet side selected option').located(by.css('.bet-option.is-selected'))
}