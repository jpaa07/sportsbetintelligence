import { Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export const LeftNavbarPage = {
    soccerBetsButton: Target.the('soccer bets button').located(by.className('nav-football')),
    bascketBetsButton: Target.the('Bascketball bets button').located(by.className('nav-basketball')),
    tennisBetsButton: Target.the('Tennis bets button').located(by.className('nav-tennis'))
}