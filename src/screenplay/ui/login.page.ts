import { Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export const LoginPage = {
    goToLoginButton: Target.the('go to login button').located(by.className('btn--submit')),
    usernameField: Target.the('username field').located(by.id('user_login')),
    passwordField: Target.the('password field').located(by.id('user_password')),
    loginButton: Target.the('login button').located(by.css('button[type="submit"]'))
}