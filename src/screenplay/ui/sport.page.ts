import { Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export const SportPage = {
    sportCompetitionsDropdow: Target.the('Sport Competition dropdown').located(by.className('competition-dpdwn')),
    filterByCompetitionButton: Target.the('filter by competition button').located(by.className('gtm-event-nav-filter-competition'))
}