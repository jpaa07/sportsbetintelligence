import { Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export const HomePage = {
    blockNotificationButton: Target.the('block notifications button').located(by.id('onesignal-popover-cancel-button')),
    closeAddsModalButton: Target.the('close adds modal button').located(by.id('hstp_20098_interstitial_pub')),
    acceptDailyBonusButton: Target.the('accept daily bonus button').located(by.cssContainingText('button','Accept'))
}