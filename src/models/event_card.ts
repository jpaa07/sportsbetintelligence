export class EventCard {

    private name: string;
    private league: string;
    private date: string;
    private played: string;
    private url: string;

    constructor() {
    };

    /**
     * Getter $name
     * @return {string}
     */
	public get $name(): string {
		return this.name;
	}

    /**
     * Getter $league
     * @return {string}
     */
	public get $league(): string {
		return this.league;
	}

    /**
     * Getter $date
     * @return {string}
     */
	public get $date(): string {
		return this.date;
	}

    /**
     * Getter $played
     * @return {string}
     */
	public get $played(): string {
		return this.played;
	}

    /**
     * Setter $name
     * @param {string} value
     */
	public set $name(value: string) {
		this.name = value;
	}

    /**
     * Setter $league
     * @param {string} value
     */
	public set $league(value: string) {
		this.league = value;
	}

    /**
     * Setter $date
     * @param {string} value
     */
	public set $date(value: string) {
		this.date = value;
	}

    /**
     * Setter $played
     * @param {string} value
     */
	public set $played(value: string) {
		this.played = value;
    }
    
    /**
     * Getter $url
     * @return {string}
     */
	public get $url(): string {
		return this.url;
	}

    /**
     * Setter $url
     * @param {string} value
     */
	public set $url(value: string) {
		this.url = value;
	}
}