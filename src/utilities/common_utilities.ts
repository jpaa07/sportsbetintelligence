import { EventCard } from "../models/event_card";
import { ElementArrayFinder, element, by, ElementFinder } from "protractor";
import { MathMethods } from "./math-methods";

export class CommonUtilities {

    public static async getEventCards(): Promise<EventCard[]> {
        const events: EventCard[] = [];
        const eventContainers: ElementArrayFinder = element.all(by.className('event-content'));
        for (const eventContainer of await eventContainers) {
            const eventCard = new EventCard();
            eventCard.$name = await (eventContainer.element(by.className('event-content__title'))).getText();
            eventCard.$date = await (eventContainer.element(by.className('event-content__time'))).getText();
            eventCard.$league = await (eventContainer.element(by.className('event-content__comp'))).getText();
            eventCard.$played = await (eventContainer.element(by.className('event-counter'))).getText();
            eventCard.$url = await (eventContainer.element(by.className('event-content__title'))).getAttribute('href');
            events.push(eventCard);
        }
        return events;
    }

    public static async getAppPredictions(): Promise<any> {
        const probabilities: number[] = [];
        const teams: string[] = [];
        const betOptions: ElementArrayFinder = element.all(by.className('bet-option'));
        for (const betOption of await betOptions) {
            const prediction = await (betOption.element(by.className('right'))).getText();
            const teamName = await (betOption.element(by.css('strong'))).getText();
            const probability = await MathMethods.getTipProbability(+prediction);
            await probabilities.push(probability);
            await teams.push(teamName);
        }

        return {
            teams: {
                homeName: teams[0],
                awayName: teams[teams.length-1]
            },
            predictions: {
                prediction : await probabilities.indexOf(Math.max(...probabilities)),
                probability: await Math.max(...probabilities)
            }
        }
    }
}