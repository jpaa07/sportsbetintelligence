import { SeasonHeadToHeadResponse } from "../api-services/soccer-response-models/season-head-to-head-response";
import { ApiBase } from "../api-services/soccer-api-base";
import { JsonAccess } from "./json-access";
import { MathMethods } from "./math-methods";

export class SoccerUtil {

    public static resultsData: number[];

    public static async getLastResultsBetweenTwoTeams(numberOfMatches: number, homeTeam: string,
        awayTeam: string, league: string): Promise<number[]> {
        let resultAndDateArray = [];
        let season = await this.getActualSeason();
        do {
            const response: SeasonHeadToHeadResponse = await ApiBase.getSeasonHeadToHead(
                league, season, homeTeam, awayTeam);

            for (let i = response.rounds.length; i-- > 0;) {
                if (resultAndDateArray.length < numberOfMatches) {
                    if (response.rounds[i].match_result != '') {
                        const winner = await this.getMatchWinner(
                            response.rounds[i].home_team, response.rounds[i].away_team, response.rounds[i].match_result);
                        if (winner == homeTeam) {
                            resultAndDateArray = await this.insertValueIntoArrayBasedOnDate(
                                resultAndDateArray, { result: RESULTS.HOME, date: response.rounds[i].date_match });
                        } else if (winner == awayTeam) {
                            resultAndDateArray = await this.insertValueIntoArrayBasedOnDate(
                                resultAndDateArray, { result: RESULTS.AWAY, date: response.rounds[i].date_match });
                        } else {
                            resultAndDateArray = await this.insertValueIntoArrayBasedOnDate(
                                resultAndDateArray, { result: RESULTS.TIE, date: response.rounds[i].date_match });
                        }
                    }
                }
            }
            season = await this.getSpecificSeasonBefore(1, season);
        } while (resultAndDateArray.length < numberOfMatches);
        const results: number[] = await this.getArrayResults(resultAndDateArray);
        return results;
    }

    public static async getTeamLastResults(numberOfMatches: number, team: string, league: string) {

        let resultAndDateArray = [];
        const teamIdentifier = await JsonAccess.getTeamIdentifier(league, team);
        let season = await this.getActualSeason();
        do {
            const response: SeasonHeadToHeadResponse = await ApiBase.getSeasonMatchesForATeam(
                league, season, teamIdentifier);
            for (let i = response.rounds.length; i-- > 0;) {
                if (resultAndDateArray.length < numberOfMatches) {
                    if (response.rounds[i].match_result != '') {
                        const winner = await this.getMatchWinner(
                            response.rounds[i].home_team, response.rounds[i].away_team, response.rounds[i].match_result);
                        if (winner == team) {
                            resultAndDateArray = await this.insertValueIntoArrayBasedOnDate(
                                resultAndDateArray, { result: RESULTS.HOME, date: response.rounds[i].date_match });
                        } else if (winner == 'tie') {
                            resultAndDateArray = await this.insertValueIntoArrayBasedOnDate(
                                resultAndDateArray, { result: RESULTS.TIE, date: response.rounds[i].date_match });
                        } else {
                            resultAndDateArray = await this.insertValueIntoArrayBasedOnDate(
                                resultAndDateArray, { result: RESULTS.AWAY, date: response.rounds[i].date_match });
                        }
                    }
                }
            }
            season = await this.getSpecificSeasonBefore(1, season);
        } while (resultAndDateArray.length < numberOfMatches);
        const results: number[] = await this.getArrayResults(resultAndDateArray);
        return results;
    }

    public static async getActualSeason(): Promise<string> {
        const today: Date = new Date();
        const month: number = today.getMonth();
        let year: number = today.getFullYear();
        year = +(year.toString().substring(2, 4));

        if (month <= 5) {
            return `${year - 1}-${year}`;
        } else {
            return `${year}-${year + 1}`;
        }
    }

    public static async getSpecificSeasonBefore(yearsBefore: number, actualSeason: string): Promise<string> {
        const seasonYears: string[] = actualSeason.split('-');
        return `${+seasonYears[0] - yearsBefore}-${+seasonYears[1] - yearsBefore}`;
    }

    public static async getMatchWinner(homeTeam: string, awayTeam: string, matchResult: string): Promise<string> {
        const scores: string[] = matchResult.split('-');
        if (+scores[0] > +scores[1]) {
            return homeTeam.toLowerCase();
        } else if (scores[0] < scores[1]) {
            return awayTeam.toLowerCase();
        } else {
            return 'tie';
        }
    }

    public static async getResultPrediction(numberOfMatches: number, homeTeam: string, awayTeam: string, league: string) {
        let homeTeamPrediction: number, homeTeamProbability, awayTeamPrediction, awayTeamProbability, matchPrediction, matchProbability;
        this.resultsData = await[];
        this.resultsData = await this.getTeamLastResults(numberOfMatches, homeTeam, league);
        homeTeamPrediction = await this.getWinPrediction();
        homeTeamProbability = await this.getWinProbability(homeTeamPrediction);
        this.resultsData = await this.getTeamLastResults(numberOfMatches, awayTeam, league);
        awayTeamPrediction = await this.getWinPrediction();
        awayTeamProbability = await this.getWinProbability(awayTeamPrediction);
        this.resultsData = await this.getLastResultsBetweenTwoTeams(numberOfMatches, homeTeam, awayTeam, league);
        matchPrediction = await this.getWinPrediction();
        matchProbability = await this.getWinProbability(matchPrediction);
        return {
            home: {
                prediction: homeTeamPrediction,
                probability: homeTeamProbability,
            },
            away: {
                prediction: awayTeamPrediction,
                probability: awayTeamProbability,
            },
            match: {
                prediction: matchPrediction,
                probability: matchProbability
            }
        }
    }

    public static async getWinProbability(winPrediction: number): Promise<number> {
        let probabilities = await this.getProbabilies();
        if (parseInt(probabilities.home) == 100 && parseInt(probabilities.tie) == 100 && parseInt(probabilities.away) == 100) {
            return 100;
        } else {
            switch (winPrediction) {
                case 0:
                    return (100 - parseInt(probabilities.home));
                case 1:
                    return (100 - parseInt(probabilities.tie));
                default:
                    return (100 - parseInt(probabilities.tie));
            }
        }
    }

    private static async getProbabilies() {
        let mean = await MathMethods.getMean(this.resultsData);
        let standardDeviation = await MathMethods.getStandardDeviation(this.resultsData);
        return {
            home: await MathMethods.normalDistribution(RESULTS.HOME, mean, standardDeviation),
            tie: await MathMethods.normalDistribution(RESULTS.TIE, mean, standardDeviation),
            away: await MathMethods.normalDistribution(RESULTS.AWAY, mean, standardDeviation)
        }
    }

    public static async getWinPrediction() {
        let cont: number = this.resultsData.length;
        let arrayX: number[] = [];
        for (let index = 0; index < cont; index++) {
            arrayX[index] = index;
        }
        let lr = await MathMethods.linearRegression(arrayX, this.resultsData);
        let valuePredicted = lr.slope * (cont + 1) + lr.intercept;
        valuePredicted = Math.round(valuePredicted);
        valuePredicted = valuePredicted <= 0 ? valuePredicted = 0 : valuePredicted;
        valuePredicted = valuePredicted > 2 ? valuePredicted = 2 : valuePredicted;
        return valuePredicted;
    }

    public static async getTeamsProbabilities(homeTip: number, tieTip: number, awayTip) {
        const homeProbability = MathMethods.getTipProbability(homeTip);
        const tieProbability = MathMethods.getTipProbability(tieTip);
        const awayProbability = MathMethods.getTipProbability(awayTip);
        return {
            home: homeProbability,
            tie: tieProbability,
            way: awayProbability
        }
    }

    public static async insertValueIntoArrayBasedOnDate(resultsArray: Array<any>, resultToInsert: any): Promise<number[]> {

        resultToInsert.date = new Date(resultToInsert.date);

        if (resultsArray.length == 0) {
            resultsArray.push(resultToInsert);
        } else {
            for (let i = 0; i < resultsArray.length; i++) {
                resultsArray[i].date = new Date(resultsArray[i].date);
                if (resultToInsert.date < resultsArray[i].date) {
                    resultsArray.splice(i, 0, resultToInsert);
                    return resultsArray;
                }
            }
            await resultsArray.push(resultToInsert);
        }
        return resultsArray;
    }

    public static async getArrayResults(resultAndDateArray: any[]): Promise<number[]> {
        const resultsArray: number[] = [];
        for (const resultObject of resultAndDateArray) {
            resultsArray.push(resultObject.result);
        }
        return resultsArray;
    }

    public static async getFinalResult(setProbability: number,...results: any[]): Promise<number> {
        let probabilities = [[0,0],[0,0],[0,0]];
        
        for (const result of results){
            probabilities[result.prediction][0] += result.probability;
            probabilities[result.prediction][1] += 1;
        }

        let probabilitiesSum = 0;
        let pointsCount = 0;
        let maxIndex;
        for (const probability of probabilities) {
            
            if(probability[1] > 0) {
                const index = probabilities.indexOf(probability);
                if (probabilities[index][0] > probabilitiesSum) {
                    probabilitiesSum = probabilities[index][0];
                    pointsCount = probabilities[index][1];
                    maxIndex = index;
                }
            }
        }
        
        const finalProbability = (probabilitiesSum / pointsCount);
        if (finalProbability >= setProbability) {
            return maxIndex;
        } else {
            return -1;
        }
    }
}

export enum RESULTS {
    HOME = 0,
    TIE = 1,
    AWAY = 2
}