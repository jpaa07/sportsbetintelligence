export class MathMethods {

    public static async getMean(numbers: number[]): Promise<number> {
        let total: number = 0;
        for (let i: number = 0; i < numbers.length; i += 1) {
            total += numbers[i];
        }
        return total / numbers.length;
    }
    
    public static async getStandardDeviation(numbers: number[]): Promise<number> {
        const average = (data) => data.reduce((sum, value) => sum + value, 0) / data.length;
        const avg = average(numbers);
        const diffs = numbers.map((value) => value - avg);
        const squareDiffs = diffs.map((diff) => diff * diff);
        const avgSquareDiff = average(squareDiffs);
        return Math.sqrt(avgSquareDiff);
    }
    
    public static async normalDistribution(parameter: number, mean: number, standardDeviation: number) {
        let probability: number;
        if (standardDeviation < 0) {
            console.log('The standard deviation must be nonnegative.');
        } else if (standardDeviation == 0) {
            if (parameter < mean) {
                probability = 0;
            } else {
                probability = 1;
            }
        } else {
            probability = await this.normalcdf((parameter - mean) / standardDeviation);
            probability = await Math.round(100000 * probability) / 100000;
        }
        return (probability * 100).toFixed();
    }
    
    public static async normalcdf(value: number) {
        let T = 1 / (1 + .2316419 * Math.abs(value));
        let D = .3989423 * Math.exp(-value * value / 2);
        let probability = D * T * (.3193815 + T * (-.3565638 + T * (1.781478 + T * (-1.821256 + T * 1.330274))));
        if (value > 0) {
            probability = 1 - probability;
        }
        return probability;
    }
    
    public static async linearRegression(x: number[], y: number[]) {
        let n = y.length;
        let sum_x = 0;
        let sum_y = 0;
        let sum_xy = 0;
        let sum_xx = 0;
        let sum_yy = 0;
        for (let i: number = 0; i < y.length; i++) {	
            sum_x += x[i];
            sum_y += y[i];
            sum_xy += (x[i]*y[i]);
            sum_xx += (x[i]*x[i]);
            sum_yy += (y[i]*y[i]);
        }
        let slope = (n * sum_xy - sum_x * sum_y) / (n*sum_xx - sum_x * sum_x);
        let intercept = (sum_y - slope * sum_x)/n;
        return {
            slope: slope,
            intercept: intercept
        }
    }

    public static async getTipProbability(tip: number) {
        return 100-((tip*100)-100);
    }
}