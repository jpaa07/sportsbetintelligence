
import * as fs from 'fs';

export class JsonAccess {
    
    private static soccerData = JSON.parse(fs.readFileSync('./data/soccer.data.json', 'utf8'));
    private static usersData = JSON.parse(fs.readFileSync('./data/users.data.json', 'utf8'));
    private static appLinksData = JSON.parse(fs.readFileSync('./data/app-links.data.json', 'utf8'));


    public static async getTeamIdentifier(league: string, team: string): Promise<string> {
        const leagueInfo = this.soccerData.leagues.find(c => c.name === league);
        const teamInfo = leagueInfo.teams.find(c => c.team_slug === team);
        return teamInfo.identifier;
    }

    public static async getLoginUrl(): Promise<string> {
        return this.appLinksData.login;
    }

    public static async getActorCredentials(actorName: string, sport: string): Promise<any> {
        const sportInfo = this.usersData.sports.find(c => c.name === sport);
        const account = sportInfo.accounts.find(c => c.username === actorName);
        return {
            'email': account.email,
            'password': account.password
        };
    }

    public static async getPFBcompetitionName(apiCompetitionName: string): Promise<string> {
        const leagueInfo = this.soccerData.leagues.find(c => c.name === apiCompetitionName);
        return leagueInfo.playfulbet_name;
    }

    public static async getApiTeamName(league: string, pfbTeam): Promise<string> {
        const leagueInfo = this.soccerData.leagues.find(c => c.name === league);
        const teamInfo = leagueInfo.teams.find(c => c.PFB_name === pfbTeam);
        return teamInfo.team_slug;
    }

    public static async  getInactiveTeams(league: string): Promise<string[]> {
        const inactiveTeams: string[] = [];
        const leagueInfo = this.soccerData.leagues.find(c => c.name === league);
        const teams: any[] = leagueInfo.teams;
        for (const team of teams) {
            if(team.active == 0) {
                inactiveTeams.push(team.PFB_name);
            }
        }
        return inactiveTeams;
    }
}